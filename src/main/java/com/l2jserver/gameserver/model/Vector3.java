package com.l2jserver.gameserver.model;

public class Vector3 {

    public float x;
    public float y;
    public float z;


    public Vector3()
    {
        x = 0;
        y = 0;
        z = 0;
    }


    public Vector3(int X, int Y, int Z)
    {
        x = (float) X;
        y = (float) Y;
        z = (float) Z;
    }


    public Vector3(Location location)
    {
        x = (float) location.getX();
        y = (float) location.getY();
        z = (float) location.getY();
    }


    public Vector3(Vector3 v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    public Vector3(Location from, Location to)
    {
        x = (float) (to.getX() - from.getX());
        y = (float) (to.getY() - from.getY());
        z = (float) (to.getZ() - from.getZ());
    }

    public void set(int X, int Y, int Z)
    {
        x = (float) X;
        y = (float) Y;
        z = (float) Z;
    }

    public void set(Vector3 v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    public void invert()
    {
        x *= -1;
        y *= -1;
        z *= -1;
    }

    public double length()
    {
        return Math.sqrt(x*x + y*y + z*z);
    }

    public double squareLength()
    {
        return x*x + y*y + z*z;
    }


    public void normalize()
    {
        double len = length();
        x =  x / (float) len;
        y =  y / (float) len;
        z =  z / (float) len;
    }

    public void normalize(double len)
    {
        x =  x / (float) len;
        y =  y / (float) len;
        z =  z / (float) len;
    }

    public void multiply(float k)
    {
        x = x * k;
        y = y * k;
        z = z * k;
    }

    public void add(Location location)
    {
        x += location.getX();
        y += location.getY();
        z += location.getZ();
    }

    public Location toLocation()
    {
        return new Location((int) x, (int) y, (int) z);
    }
}
